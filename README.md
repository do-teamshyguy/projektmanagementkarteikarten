# ProjektManagementKarteikarten
Dieses Repository enthält Karteikarten im Crowd-Anki Format für die Veranstaltung Projektmanagemt im Master der TU-Dortmund für das Wintersemester 2023/2024.

## Vorgehen
1. Ankiweb installieren https://apps.ankiweb.net/
    * Linux: [Flatpak](https://flathub.org/en-GB/apps/net.ankiweb.Anki) oder Paketmanager
    * Andere Betriebbsysteme: Bitte ergänzen.
2. Das CrowdAnki Plugin installieren:  
    https://ankiweb.net/shared/info/1788670778 
    * Menüpunkt "Extras" > "Erweiterungen" 
    * Dann "Erweiterungen installieren"
    * Code `1788670778` benutzen
    * Anki neu starten
2. Repo klonen:
     * `git clone git@gitlab.com:do-teamshyguy/projektmanagementkarteikarten.git` 
     * oder über https `git clone https://gitlab.com/do-teamshyguy/projektmanagementkarteikarten.git`

3. Deck importieren:  
    Menüpunkt "Datei" > "CrowdAnki: Import from disk"

## Wie ändere ich Karten oder mache neue?
Im Issue gucken, welche Karten noch fehlen:
https://gitlab.com/do-teamshyguy/projektmanagementkarteikarten/-/issues/1

1. Zuerst `git pull` damit man die Änderungen der anderen bei sich hat.
2. Die Karteikarten nochmals aus dem Verzeichnis importieren, damit man die neuen Karten hat. 
3. Neue Karten erstellen/Karten verändern
4. Die Karteikarten über "Datei" > "Exportieren", Format "CrowdAnki JSON represantation" wählen. Als Stapel nur diesen. Dann exportieren.  
**Wichtig** nun muss das geklonte Repo als Ordner ausgewählt werden, nicht der `ProjektManagementKarteikarten` Unterordner. Ansonsten wird darin nochmals ein Unterordner erstellt.
5. 'git commit', damit die Änderungen für den push vorbereitet werden.
6. `git push`
7. Im Issue https://gitlab.com/do-teamshyguy/projektmanagementkarteikarten/-/issues/1 abhaken, welche man gemacht hat
